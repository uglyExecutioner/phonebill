/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tariff2', {
    slno: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    code: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    location: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    pulse: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    rateperpulse: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    tariffcode: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tariff2'
  });
};
