const express = require('express');
const sequelize = require('sequelize');
const call_details = require('./models/call_details');
const route = require("./routes/routes");
const bodyParser = require('body-parser');
const path = require('path');
const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/", express.static(path.join(__dirname,"angular")));
app.use("logFile", express.static(path.join(__dirname,"logFile")));

// parse application/json
app.use(bodyParser.json())
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers",
    "Origin,X-Requested-With, Content-Type, Accept");
    res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, PATCH, DELETE, OPTIONS"
    );
    next();
  });
app.use(route);
app.use((req,res, next) =>{
    res.sendFile(path.join(__dirname,"angular","index.html"));
  })
module.exports = app;
