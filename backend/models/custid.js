/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('custid', {
    extension: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    id: {
      type: DataTypes.STRING(20),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'custid'
  });
};
