/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('rec_type', {
    REC_TYPE_ID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    REC_TPE_NAME: {
      type: DataTypes.STRING(30),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'rec_type'
  });
};
