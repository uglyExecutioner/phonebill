/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('employ', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    salary: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    name: {
      type: DataTypes.CHAR(20),
      allowNull: true
    },
    post: {
      type: DataTypes.CHAR(15),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'employ'
  });
};
