/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('testint', {
    telephone_no: {
      type: DataTypes.STRING(70),
      allowNull: true
    },
    extn_no: {
      type: DataTypes.STRING(30),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'testint'
  });
};
