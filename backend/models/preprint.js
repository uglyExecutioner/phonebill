/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('preprint', {
    header: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    object_id: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    m_left: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    m_right: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    m_top: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    m_bottom: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    width: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    fontsize: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'preprint'
  });
};
