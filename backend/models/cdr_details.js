/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cdr_details', {
    accesscodestart: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    accesscodewidth: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    accountcodestart: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    accountcodewidth: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    callednumberstart: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    callednumberwidth: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    callingnumberstart: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    callingnumberwidth: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    dateformat1: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    datestart: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    datewidth: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    durationformat: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    durationstart: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    durationwidth: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    extensionstart: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    extensionwidth: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    incomingformat: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    incomingstart: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    incomingtransferformat: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    incomingtransferstart: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    incomingtransferwidth: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    incomingwidth: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    outgoingformat: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    outgoingstart: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    outgoingtransferformat: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    outgoingtransferstart: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    outgoingtransferwidth: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    outgoingwidth: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    ringdurationformat: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    ringdurationstart: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    ringdurationwidth: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    timeformat: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    timestart: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    timewidth: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    trunkstart: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    trunkwidth: {
      type: DataTypes.STRING(20),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'cdr_details'
  });
};
