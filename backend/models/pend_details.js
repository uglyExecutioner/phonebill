/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pend_details', {
    ext_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    billamount: {
      type: DataTypes.DOUBLE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'pend_details'
  });
};
