/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('department', {
    deptcode: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    deptname: {
      type: DataTypes.STRING(30),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'department'
  });
};
