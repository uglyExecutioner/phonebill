/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('priextension', {
    prino: {
      type: DataTypes.STRING(16),
      allowNull: true
    },
    priname: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    designation: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    address: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    contactno: {
      type: DataTypes.STRING(16),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'priextension'
  });
};
