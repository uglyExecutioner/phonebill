/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('valuesettings', {
    plan_name: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    rentals: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    service_charges: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    taxes: {
      type: DataTypes.STRING(15),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'valuesettings'
  });
};
