/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ext_summary_report', {
    extension: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    Local_call_count: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: "0"
    },
    Local_call_Duration: {
      type: DataTypes.TIME,
      allowNull: true,
      defaultValue: "00:00:00"
    },
    Local_Call_Cost: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      defaultValue: "0"
    },
    Std_Call_Count: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: "0"
    },
    Std_Call_Duration: {
      type: DataTypes.TIME,
      allowNull: true,
      defaultValue: "00:00:00"
    },
    Std_Call_Cost: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      defaultValue: "0"
    },
    Local_Mobile_Count: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: "0"
    },
    Local_Mobile_Duration: {
      type: DataTypes.TIME,
      allowNull: true,
      defaultValue: "00:00:00"
    },
    Local_Mobile_Cost: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      defaultValue: "0"
    },
    Std_Mobile_Count: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: "0"
    },
    Std_Mobile_Duration: {
      type: DataTypes.TIME,
      allowNull: true,
      defaultValue: "00:00:00"
    },
    Std_Mobile_Cost: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      defaultValue: "0"
    },
    Isd_Count: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: "0"
    },
    Isd_Duration: {
      type: DataTypes.TIME,
      allowNull: true,
      defaultValue: "00:00:00"
    },
    Isd_Cost: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      defaultValue: "0"
    }
  }, {
    sequelize,
    tableName: 'ext_summary_report'
  });
};
