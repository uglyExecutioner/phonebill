/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('re_receipt', {
    RECR_ID: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    EXTR_ID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    BILLR_ID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    RECR_TYPE_ID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    RECR_AMT: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    RECR_DATE: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    RECR_PAY_MODE: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    RECR_PARTICULARS: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    RECR_REMARKS: {
      type: DataTypes.STRING(300),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 're_receipt'
  });
};
