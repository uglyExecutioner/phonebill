/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('bill', {
    bill_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    ext_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    rec_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    bill_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    bill_period_from: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    bill_period_to: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    bill_pay_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    net_calls: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    net_amount: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    bill_amount: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    misc_charge: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    service_taxes: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    bill_status: {
      type: DataTypes.STRING(10),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'bill'
  });
};
