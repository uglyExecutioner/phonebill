/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('othercharges', {
    RID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    EXT_ID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    DATE: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    Amount: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    PAYMODE: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    PARTICULARS: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    REMARKS: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    REC_TYPE: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'othercharges'
  });
};
