/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('i_customer', {
    consumer_no: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    name_address: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    plan_name: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    device: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    begin_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    modem_rent: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    modem_price: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    mobile_no: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    email_id: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    disconnected: {
      type: DataTypes.STRING(5),
      allowNull: true
    },
    disconnectedDate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    sl_no: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    security_deposit: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'i_customer'
  });
};
