/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tbl_tariff', {
    code: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    lpulse: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    lrate: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    mpulse: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    mrate: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    spulse: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    srate: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    ipulse: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    irate: {
      type: DataTypes.DOUBLE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tbl_tariff'
  });
};
