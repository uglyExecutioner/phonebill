/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('freegroup', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    MobNo: {
      type: DataTypes.STRING(25),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'freegroup'
  });
};
