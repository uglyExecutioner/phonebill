/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('discount', {
    bill_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    rec_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    ext_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    disc_percentage: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    disc_amt: {
      type: DataTypes.DOUBLE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'discount'
  });
};
