/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('due', {
    extnno: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    amt: {
      type: DataTypes.FLOAT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'due'
  });
};
