/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('reconnection', {
    ext_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(15),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'reconnection'
  });
};
