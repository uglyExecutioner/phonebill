/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('trunk', {
    trunkcode: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    trunkname: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    tariffcode: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'trunk'
  });
};
