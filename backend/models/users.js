/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users', {
    uid: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    usname: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    loginname: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    type: {
      type: DataTypes.STRING(5),
      allowNull: true
    },
    phoneno: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(25),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'users'
  });
};
