import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar} from '@angular/material';
export interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private http:HttpClient,private _snackBar: MatSnackBar) { }
  foods: Food[] = [
    {value: 'shantiniketan', viewValue: 'Shantiniketan'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];
  progressBar : boolean;
  title = 'frontend';
  userForm: FormGroup;
  fileName: string;
  ngOnInit(){
    this.progressBar = true;
    this.userForm =new FormGroup({
      'db' : new FormControl(null,Validators.required),
      'file' : new FormControl(null,Validators.required)
    });

  }

  onFileChange(event: Event){
    const file = (event.target as HTMLInputElement).files[0];
    const fileType = 'uploadLogFile';
    this.userForm.patchValue({file: true});
    this.userForm.get('file').updateValueAndValidity();
    this.uploadFiles(fileType,file);

  }
   uploadFiles(fileContent,file: File){
    const uploadFile = new FormData();
    if(fileContent == 'uploadLogFile'){
      uploadFile.append(fileContent, file as File);
      this.http.post("http://localhost:3000/api/upload",uploadFile)
        .subscribe((files) =>{
          this.fileName = files[0].originalname;
          console.log("log Sent",files);
        });
      }

  }



  onSubmit(){
    this.progressBar = false;
    let validForm = false;
    if(this.userForm.valid){
       validForm = true;
    }else{
      console.log("invalid form data");
      return;
    }
    if(validForm){
      console.log("File location",this.fileName)
      const data = {db:this.userForm.value.db,file:this.fileName};
      console.log(data)

      this.http.post("http://localhost:3000/shantiniketan/insert",data)
        .subscribe((response) =>{
          if(response){
            this.progressBar = true;
            this._snackBar.open("Insert to db Sucessful",'ok',{
              duration: 2000
            });
          }else{
            this._snackBar.open("Insert to db failed",'ok',{
              duration: 2000
            });
          }
        });
    }
  }
}

