/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('refundable', {
    receipt_no: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    receipt_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    extno: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    customer: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    pay_type: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    amount_paid: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'refundable'
  });
};
