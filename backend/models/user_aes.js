/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_aes', {
    user_name: {
      type: DataTypes.STRING(16),
      allowNull: true
    },
    password: {
      type: 'BLOB',
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'user_aes'
  });
};
