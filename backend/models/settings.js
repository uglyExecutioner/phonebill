/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('settings', {
    skey: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    svalue: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    scharvalue: {
      type: DataTypes.STRING(50),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'settings'
  });
};
