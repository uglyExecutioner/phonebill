/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cable_bill', {
    bill_no: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    consumer_no: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    bill_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    bill_period_from: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    bill_period_to: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    bill_pay_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    bill_amount: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(20),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'cable_bill'
  });
};
