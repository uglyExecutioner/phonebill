/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('receipt', {
    REC_ID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    EXT_ID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    BILL_ID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    REC_TYPE_ID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    REC_AMT: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    REC_DATE: {
      type: DataTypes.DATEONLY,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'receipt'
  });
};
