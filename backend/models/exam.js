/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('exam', {
    uid: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    usname: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(20),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'exam'
  });
};
