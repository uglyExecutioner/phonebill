/* jshint indent: 2 */
const sequelize = require('../config/dbconfig');
const DataTypes = require('sequelize');
const tariff = sequelize.define('tariff', {
    slno: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    code: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    location: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    pulse: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    rateperpulse: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    tariffcode: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tariff'
  });
  tariff.removeAttribute('id');
module.exports = tariff;
