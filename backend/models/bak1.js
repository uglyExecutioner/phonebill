/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('bak1', {
    date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    time: {
      type: DataTypes.TIME,
      allowNull: true
    },
    extension: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    duration: {
      type: DataTypes.TIME,
      allowNull: true
    },
    trunk: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    calltype: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    callednum: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    callingnum: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    ringduration: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    accesscode: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    accountcode: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    cost: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    units: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    location: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    deptcode: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    calldistance: {
      type: DataTypes.STRING(20),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'bak1'
  });
};
