/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('rec_print', {
    phone_num: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    customer_id: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    rec_no: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    rec_date: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    pay_mode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    money: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    bank_name: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    word: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    mon_name: {
      type: DataTypes.STRING(10),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'rec_print'
  });
};
