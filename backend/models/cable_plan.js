/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cable_plan', {
    plan_name: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    m_rental: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'cable_plan'
  });
};
