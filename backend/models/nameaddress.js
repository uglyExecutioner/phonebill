/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nameaddress', {
    code: {
      type: DataTypes.STRING(10),
      allowNull: false,
      primaryKey: true
    },
    cname: {
      type: DataTypes.STRING(200),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'nameaddress'
  });
};
