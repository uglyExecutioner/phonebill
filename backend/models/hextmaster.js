/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('hextmaster', {
    seqno: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    extno: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    code: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    bigindate: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    active: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    instrcharge: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    plancode: {
      type: DataTypes.STRING(10),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'hextmaster'
  });
};
