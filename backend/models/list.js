/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('list', {
    name: {
      type: DataTypes.STRING(2000),
      allowNull: true
    },
    flat: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    block: {
      type: DataTypes.STRING(50),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'list'
  });
};
