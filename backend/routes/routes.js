const express = require('express');
const sequelize = require('sequelize');
const call_details = require('../models/call_details');
const tariff = require('../models/tariff');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const multer = require('multer');
const readline = require('readline');
const moment = require('moment')
require("moment-duration-format");
const call_distance_list = require('../utils/call_distance_list.json');


const MIME_TYPE_MAP = {
    'image/png': 'png',
    'image/jpeg': 'jpg',
    'image/jpg': 'jpg',
    'image/jgeg': 'jpg',
    'application/pdf': 'pdf',
    'application/docx': 'docx',
    'application/octet-stream':'log'
  }


const storage = multer.diskStorage({
    destination: (req,file, cb) => {
      const isValid = new Error("invalid mime type");
      if(isValid){
        error = null;
      }
      cb(error, "backend/logFile");
    },
    filename: (req, file, cb) =>{
      console.log("fileName",file.originalname);
      console.log("file Ext",file.mimetype)
      console.log("file feield name",file.fieldname)
      const name = file.originalname.toLowerCase().split(' ').join('-');
      console.log("name of file",name);
      const ext = MIME_TYPE_MAP[file.mimetype];
      console.log(ext)
      cb(null, name);
    }
  })


router.post('/api/upload', multer({
    storage: storage,
    limits: { fileSize: 5 * 1024 * 1024}
  }).fields([
    {name:"uploadLogFile", maxCount: 1}]), (req,res,next) =>{
      const data = req.body;
      const file = req.files;
      console.log("data in body",data);
      console.log("file name",req.files);
      const url = req.protocol + '://' + req.get("host");
      console.log("Url ", url);
      if(file.uploadLogFile){
        return res.json(file.uploadLogFile);
      }
      console.log("file Uploaded");
  });
  

router.post('/shantiniketan/insert',async (req,res) =>{
    try{
        const postData = req.body;
        console.log("REQUEST BODY",postData)
        const filePath = postData.file;
        console.log("typeof",typeof filePath);
        var text = fs.readFileSync(filePath,'utf8')
        console.log("LOCATION",postData.file);
        array = text.split("\n")
        var dataArray = [];
        for(var i=0; i<array.length; i++){
        if(array[i] == ''){continue}
        let tempArray = []
        tempArray = array[i].split(",");
        dataArray.push(tempArray)
        };

        json = {};
        var c = 1;
        //console.log(dataArray)
        dataArray.forEach( (e1) =>{
        isdate = true;
        var tempjson = {};
        e1.forEach( (e2) =>{
            var key;
             if(isdate )  {
                 key = 'data';
                 tempjson[key] = e2;
                 isdate = false;
             }
             else{
                var arr = e2.split("=");
                key  = arr[0].trim();
                tempjson[key] = arr[1];
            }
        })
        json[c] = tempjson;
        c++
        });
        //console.log(json);
        const len = Object.keys(json).length;
        const val_length = Object.keys(json['2'].data.split("\t")).length;
        console.log(len,val_length)
        const call_details_data = await call_details.findAll();
        let payload ={};
        function getTariff(callednum){
            return new Promise(async(resolve) =>{
                const tariffData = await tariff.findOne({where:{
                    code: callednum.charAt(0)
                },raw:true});
                //console.log(tariffData)
                resolve(tariffData);
            })
        }
        function calculateUnit(duration,tariff){
            return new Promise(async(resolve)=>{
              if(tariff.pulse >= duration){
                  resolve(tariff.rate);
              }
              let i = tariff.pulse;//60
              let k =1;
              let j = 1;
              while(k < duration){//197
                  k = i * j;
                  j++;
              }
              let l = j-1;
              console.log("Final Unit",l)
              resolve(l);
            })
        }
        function getCallDistance(extension){
            return new Promise(async(resolve)=>{
                const cdl = call_distance_list.shantiniketan.call_distance_column;
                console.log(typeof extension)
                console.log(typeof cdl.isd)
                if(extension.startsWith(cdl.local_call) || extension.startsWith(cdl.other) || extension.startsWith(cdl.other1) || extension.startsWith(cdl.other2) ||extension.startsWith(cdl.other3)){
                    resolve("Local Call")
                }
                if(extension.startsWith(cdl.isd)){
                    resolve("Isd");
                }
                if(extension.startsWith(cdl.local)){
                    resolve("Local Call");
                }
                if(extension.startsWith(cdl.std_mobile1) || extension.startsWith(cdl.std_mobile2)){
                    resolve("Std Mobile");
                }
                if(extension.startsWith(cdl.local_mobile) || extension.startsWith(cdl.local_mobile1) || extension.startsWith(cdl.local_mobile2)){
                    resolve("Local Mobile");
                }
                if(extension.startsWith(cdl.std)){
                    resolve("Std Call");
                }
                if(extension.startsWith(cdl.edc)){
                    resolve("EDC Call");
                }
            })
        }
        let tariffDb = {};
        for(let i =1 ;i<len;i++){
            for(let j =0;j<val_length;j++){
                //console.log(json[i].data.split("\t")[j]);
                if(j == 1){
                    payload.extension = parseInt(json[i].data.split("\t")[j].slice(1))
                }
                if(j ==3){
                    payload.trunk = json[i].data.split("\t")[j]
                }   
                 if(j ==4){
                    const costCal = await getTariff(json[i].data.split("\t")[j]);
                    payload.location = costCal.location;
                    tariffDb.pulse = costCal.pulse;
                    tariffDb.rate = costCal.rateperpulse;
                    payload.callednum = json[i].data.split("\t")[j];
                    const dist = await getCallDistance(json[i].data.split("\t")[j]);
                    console.log(dist)
                    payload.calldistance = dist;
                 }
                 if(j==5){
                    payload.date = moment(json[i].data.split("\t")[j],"DD-MM-YY").format("YYYY-MM-DD")
                 }
                 if(j == 6){
                    payload.time= moment(json[i].data.split("\t")[j],"HH:mm:ss").format("HH:mm:ss")
                 }
                 if( j ==7){
                    const unit = await calculateUnit(json[i].data.split("\t")[j],tariffDb);
                    payload.units = unit;
                    payload.cost = unit.toFixed(2);
                    payload.duration = moment.duration(json[i].data.split("\t")[j],'seconds').format("hh:mm:ss",{ trim: false });
                    payload.calltype = "outgoing";
                 }
            };
            const insert = await call_details.create(payload);
            console.log(insert)
            
        }
        res.status(200).json({message:"Data inserted to db sucessfully"});
        console.log(payload)
        
        //console.log(call_details_data);

    }catch(e){
        res.status(403).json({message:"failed at inserting",error:e});
        console.log(e)
    }
   
  })



  module.exports = router;