/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ext', {
    EXTNO: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    EXTNAME: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    DEPTNAME: {
      type: DataTypes.STRING(30),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'ext'
  });
};
