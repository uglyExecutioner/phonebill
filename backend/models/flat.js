/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('flat', {
    name: {
      type: DataTypes.STRING(2000),
      allowNull: true
    },
    extension: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    flat: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'flat'
  });
};
