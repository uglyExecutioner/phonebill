/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('extcostsort', {
    Ext: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    No: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    Date: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Time: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Duration: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Calltype: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    CalledNumb: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    Units: {
      type: DataTypes.STRING(5),
      allowNull: true
    },
    Location: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    Cost: {
      type: DataTypes.STRING(10),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'extcostsort'
  });
};
