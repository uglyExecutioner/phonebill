/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('planmaster', {
    code: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    Rent: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    FreeAmt: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    Rate: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    CLI: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    HitechCall: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    FreeLocalTataCall: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    Name: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    ServiceTax: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    IntercomLimit: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    IntercomPulse: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    FreeType: {
      type: DataTypes.STRING(25),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'planmaster'
  });
};
