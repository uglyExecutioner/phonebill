/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('bak', {
    bill_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    rec_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    ext_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    amt: {
      type: DataTypes.DOUBLE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'bak'
  });
};
