/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('accountcode', {
    accountcode: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    accountname: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    extension: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'accountcode'
  });
};
