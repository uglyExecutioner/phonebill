/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('freemobilecode', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    code: {
      type: DataTypes.STRING(10),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'freemobilecode'
  });
};
