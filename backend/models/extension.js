/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('extension', {
    extno: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    extname: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    deptcode: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    tariffcode: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'extension'
  });
};
