/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('total', {
    receipt_no: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    rec_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    extno: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    cust_name: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    pay_type: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    pay_mode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    amount: {
      type: DataTypes.DOUBLE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'total'
  });
};
