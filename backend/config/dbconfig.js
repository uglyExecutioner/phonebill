const Sequelize = require('sequelize');
const pgconfig = require('./config');

const sequelize = new Sequelize(pgconfig.PG_CONFIG.database, pgconfig.PG_CONFIG.user, pgconfig.PG_CONFIG.password, {
    host: pgconfig.PG_CONFIG.host,
    dialect: pgconfig.PG_CONFIG.dialect,
    define: {
        timestamps: false
    },
    pool: {
      max: 10,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  })

module.exports = sequelize;