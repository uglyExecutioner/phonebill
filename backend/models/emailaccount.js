/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('emailaccount', {
    username: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    password: {
      type: 'BLOB',
      allowNull: true
    },
    host: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    protocol: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    port: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'emailaccount'
  });
};
