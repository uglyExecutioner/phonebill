/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('phonebook', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    number: {
      type: DataTypes.STRING(16),
      allowNull: true
    },
    address: {
      type: DataTypes.STRING(200),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'phonebook'
  });
};
