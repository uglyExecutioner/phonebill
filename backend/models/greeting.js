/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('greeting', {
    GID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    MESSAGE: {
      type: DataTypes.STRING(500),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'greeting'
  });
};
