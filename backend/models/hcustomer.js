/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('hcustomer', {
    code: {
      type: DataTypes.STRING(10),
      allowNull: false,
      defaultValue: "",
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    Address1: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    Address2: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    Address3: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    city: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    phone: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    mobile: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    extnno: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'hcustomer'
  });
};
