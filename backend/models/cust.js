/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('cust', {
    sl_no: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    customer_no: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    telephone_no: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    name_address: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    type: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    rentals: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    service_charges: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    taxes: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    debits: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    credits: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    begin_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    security_deposit: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    modem_rent: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    modem_price: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    data_usage: {
      type: DataTypes.STRING(20),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'cust'
  });
};
