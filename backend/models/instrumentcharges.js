/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('instrumentcharges', {
    ID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    EXT_ID: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    DATE: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    AMOUNT: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    PAYMODE: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    PARTICULARS: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    REMARKS: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'instrumentcharges'
  });
};
