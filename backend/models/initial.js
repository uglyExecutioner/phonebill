/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('initial', {
    refundable: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    nonrefundable: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    instcharges: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    extn: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'initial'
  });
};
