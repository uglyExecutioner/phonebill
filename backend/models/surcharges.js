/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('surcharges', {
    amout: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    surchar: {
      type: DataTypes.BIGINT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'surcharges'
  });
};
