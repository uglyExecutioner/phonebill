/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('activecustomer', {
    slno: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    code: {
      type: DataTypes.STRING(10),
      allowNull: false,
      defaultValue: ""
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    extnno: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: "0"
    },
    act_date: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    deposit: {
      type: DataTypes.DOUBLE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'activecustomer'
  });
};
